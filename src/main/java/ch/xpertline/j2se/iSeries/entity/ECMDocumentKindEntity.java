package ch.xpertline.j2se.iSeries.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author dvcuong
 */
@Entity
@Table(name = "ECM_DKIND")
public class ECMDocumentKindEntity implements Serializable {
    
    @Id
    @Column(name = "kindId", nullable = false,length = 200)
    private String kindId;
    
    @Column(name = "kindDesc",nullable = false,length = 200)
    private String kindDesc;
     
    @Column(name = "env",columnDefinition="char(10)",nullable = true)
    private String env;

    /**
     * @return the env
     */
    public String getEnv() {
        return env;
    }

    /**
     * @param env the env to set
     */
    public void setEnv(String env) {
        this.env = env;
    }

    /**
     * @return the kindId
     */
    public String getKindId() {
        return kindId;
    }

    /**
     * @param kindId the kindId to set
     */
    public void setKindId(String kindId) {
        this.kindId = kindId;
    }

    /**
     * @return the kindDesc
     */
    public String getKindDesc() {
        return kindDesc;
    }

    /**
     * @param kindDesc the kindDesc to set
     */
    public void setKindDesc(String kindDesc) {
        this.kindDesc = kindDesc;
    }
       
   

}
