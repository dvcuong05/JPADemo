package ch.xpertline.j2se.iSeries.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The business data entity
 *
 * @author vnbah
 */
@Entity
@Table(name = "framework_security_business_data")
@NamedQueries({
    @NamedQuery(name = "BusinessDataEntity.getAllSoftwareCode", query = "SELECT DISTINCT ba.softwareCode FROM BusinessDataEntity ba"),
    @NamedQuery(name = "BusinessDataEntity.getDistinctKeys", query = "SELECT DISTINCT ba.softwareCode, ba.objectType, ba.objectSubType FROM BusinessDataEntity ba")
})
@XmlRootElement
public class BusinessDataEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long businessDataId;
    @Column(name = "softwareCode", nullable = true)
    private String softwareCode;
    @Column(name = "objectType", nullable = true)
    private String objectType;
    @Column(name = "objectSubType", nullable = true)
    private String objectSubType;
    @Column(name = "dataCode", nullable = true)
    private String dataCode;
    @Column(name = "dataType", nullable = true)
    private String dataType;
    @Column(name = "dataField", nullable = true)
    private String dataField;
    @Column(name = "isCustomDefined", nullable = true)
    private Boolean isCustomDefined;
    @Column(name = "solrFieldName", nullable = true)
    private String solrFieldName    ;
    @Column(name = "databaseEntityName", nullable = true)
    private String databaseEntityName;
    @Column(name = "databaseFieldName", nullable = true)
    private String databaseFieldName;
    @Column(name = "eventDataJSONTag", nullable = true)
    private String eventDataJSONTag;
    
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "businessDataId")
    private List<BusinessDataNameEntity> businessDataNameEntity;
    
    public BusinessDataEntity() {
    }
    
    public BusinessDataEntity(Long id) {
        this.businessDataId = id;
    }

    public String getDataCode() {
        return dataCode;
    }

    public void setDataCode(String dataCode) {
        this.dataCode = dataCode;
    }
    
    public String getDataType() {
        return dataType;
    }

    public void setDataType(String businessDataType) {
        this.dataType = businessDataType;
    }
    
    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }
    
    public Long getBusinessDataId() {
        return businessDataId;
    }

    public void setBusinessDataId(Long businessDataId) {
        this.businessDataId = businessDataId;
    }

    public String getSoftwareCode() {
        return softwareCode;
    }

    public void setSoftwareCode(String softwareCode) {
        this.softwareCode = softwareCode;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectSubType() {
        return objectSubType;
    }

    public void setObjectSubType(String objectSubType) {
        this.objectSubType = objectSubType;
    }

    public Boolean getIsCustomDefined() {
        return isCustomDefined;
    }

    public void setIsCustomDefined(Boolean isCustomDefined) {
        this.isCustomDefined = isCustomDefined;
    }
    
    public String getSolrFieldName() {
        return solrFieldName;
    }

    public void setSolrFieldName(String solrFieldName) {
        this.solrFieldName = solrFieldName;
    }

    public String getDatabaseEntityName() {
        return databaseEntityName;
    }

    public void setDatabaseEntityName(String databaseEntityName) {
        this.databaseEntityName = databaseEntityName;
    }

    public String getDatabaseFieldName() {
        return databaseFieldName;
    }

    public void setDatabaseFieldName(String databaseFieldName) {
        this.databaseFieldName = databaseFieldName;
    }

    public String getEventDataJSONTag() {
        return eventDataJSONTag;
    }

    public void setEventDataJSONTag(String eventDataJSONTag) {
        this.eventDataJSONTag = eventDataJSONTag;
    }

    public List<BusinessDataNameEntity> getBusinessDataNameEntity() {
        return businessDataNameEntity;
    }

    public void setBusinessDataNameEntity(List<BusinessDataNameEntity> businessDataNameEntity) {
        this.businessDataNameEntity = businessDataNameEntity;
    }
}
