package ch.xpertline.j2se.iSeries.entity;

import ch.xpertline.j2se.jpademo.entity.ECMParameterEntity;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author nhvtuyen
 */
public class Main {

    public static void main(String[] args) {
        try {
            
//             EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("sql");
//            EntityManager em = emFactory.createEntityManager();
//            em.getTransaction().begin();
//            List<BusinessDataNameEntity> business = findParameter(em);
//            em.getTransaction().commit();
//            em.close();
//            emFactory.close();
            JsonObject jsonObject =  new JsonObject();
            
            System.out.println("Time:"+ getDataAsString(jsonObject,"Test"));
        } catch (Exception e) {
            System.out.println("Exception - "+ e.getMessage());
        } finally {

        }

    }
    
    public static String getDataAsString(JsonObject jsonObject, String attribute) {
        if (jsonObject == null) {
            return StringUtils.EMPTY;
        }
        JsonElement jsonElement = jsonObject.get(attribute);
        if (jsonElement == null || jsonElement instanceof JsonNull) {
            return StringUtils.EMPTY;
        }
        return jsonElement.getAsString();
    }
    

    public static List<BusinessDataNameEntity> findParameter(EntityManager em) {
        Query query = em.createNamedQuery("BusinessDataEntity.getDistinctKeys");
        return query.getResultList();
    }

    public  void update(boolean filter, boolean isAs400, EntityManager em) {
        String ncString = isAs400 ? " WITH NC" : "";
        String sql = "UPDATE XDOCDTA30A.ECM_DTTPXT SET FILTER = ?, PROPAGATE = 1 WHERE ALATCD = ?" + ncString;
        Query query = em.createNativeQuery(sql);

        query.setParameter(1, filter);
        query.setParameter(2, "NIKOS");
        query.executeUpdate();
    }
    
    public static class MyCopyDirVisitor extends SimpleFileVisitor<Path> {

    private Path fromPath;
    private Path toPath;
    private StandardCopyOption copyOption;


    public MyCopyDirVisitor (Path fromPath, Path toPath, StandardCopyOption copyOption) {
        this.fromPath = fromPath;
        this.toPath = toPath;
        this.copyOption = copyOption;
    }

    public MyCopyDirVisitor (Path fromPath, Path toPath) {
        this(fromPath, toPath, StandardCopyOption.REPLACE_EXISTING);
    }

    public FileVisitResult preVisitDirectory (Path dir, BasicFileAttributes attrs)
                        throws IOException {

        Path targetPath = toPath.resolve(fromPath.relativize(dir));
        if (!Files.exists(targetPath)) {
            Files.createDirectory(targetPath);
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile (Path file, BasicFileAttributes attrs)
                        throws IOException {

        Files.copy(file, toPath.resolve(fromPath.relativize(file)), copyOption);
        return FileVisitResult.CONTINUE;
    }
}
}
