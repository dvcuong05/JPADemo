package ch.xpertline.j2se.iSeries.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ECM_KIKW")
@XmlRootElement
@IdClass(ECMKeywordTypeId.class)
@NamedQueries({
    @NamedQuery(name = "ECMKeywordTypeEntity.updateByDataTypeId", query = "UPDATE ECMKeywordTypeEntity SET keywordDescription = :keywordDescription, format = :format, "
            + "occurence = :occurence, style = :style, listType = :typeList, unique = :unique, mask = :mask, type = :type where keywordId = :keywordId"),
    @NamedQuery(name = "ECMKeywordTypeEntity.findByDataTypeId", query = "SELECT k FROM ECMKeywordTypeEntity k WHERE k.keywordId = :dataTypeId"),
    @NamedQuery(name = "ECMKeywordTypeEntity.deleteByKindId", query = "DELETE ECMKeywordTypeEntity k WHERE k.kindId = :kindId")
})
public class ECMKeywordTypeEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "env", columnDefinition = "char(10)", nullable = false)
    private String environment;

    @Id
    @Column(name = "pacId", nullable = false, length = 200)
    private String pacId;

    @Id
    @Column(name = "baseId", columnDefinition = "char(5)", nullable = false)
    private String baseId;

    @Column(name = "orgId", columnDefinition = "char(200)", nullable = true)
    private String organizationId;

    @Id
    @Column(name = "kindId", nullable = false, length = 200)
    private String kindId;

    @Column(name = "kindDesc", nullable = true, length = 200)
    private String kindDescription;

    @Id
    @Column(name = "kwId", nullable = false, length = 200)
    private String keywordId;

    @Column(name = "kwDesc", nullable = true, length = 200)
    private String keywordDescription;
    @Column(name = "format", nullable = false, length = 10)
    private String format;
    @Column(name = "occurence", nullable = true, length = 200)
    private String occurence;
    @Column(name = "mandatory", nullable = true)
    private Boolean mandatory;
    @Column(name = "length", nullable = true)
    private Integer length;
    @Column(name = "visiAddDoc", nullable = true)
    private Boolean visibleAddDoc;
    @Column(name = "searchDoc", nullable = true)
    private Boolean searchDoc;
    @Column(name = "readOnly", nullable = true)
    private Boolean readOnly;
    @Column(name = "style", nullable = true, length = 200)
    private String style;
    @Column(name = "kwunique", nullable = true)
    private Boolean unique;
    @Column(name = "publish", nullable = true)
    private Boolean publish;
    @Column(name = "type", nullable = true, length = 200)
    private String type;
    @Column(name = "mask", nullable = true, length = 200)
    private String mask;
    @Column(name = "mapping", nullable = true, length = 200)
    private String mapping;
    @Column(name = "lsttype", nullable = true, length = 200)
    private String listType;
    @Column(name = "operator", nullable = true, length = 200)
    private String operator;
    @Column(name = "frameSearc", nullable = true)
    private Boolean frameSearch;
    @Column(name = "fldTyId", nullable = true, length = 200)
    private String fldTyId;
    @Column(name = "fldTyDesc", nullable = true, length = 200)
    private String fldTyDesc;
    @Column(name = "kindGrId", nullable = true, length = 200)
    private String kindGrId;
    @Column(name = "kindGrDesc", nullable = true, length = 200)
    private String kindGrDesc;
    
    /***
     * The attribute indicates status of current datatype based on ecm_param
     */
    @Transient
    private boolean canceled;
    
    public ECMKeywordTypeEntity() {
        //Default constructor
    }
    
    public ECMKeywordTypeEntity(String environment, String pacId, String baseId, String organizationId, String kindId, String kindDescription, String keywordId, String keywordDescription, 
                              String format, String occurence, Boolean mandatory, Integer length, Boolean visibleAddDoc, Boolean searchDoc, Boolean readOnly, String style, 
                              Boolean unique, Boolean publish, String type, String mask, String mapping, String listType, String operator, Boolean frameSearch, String fldTyId,
                              String fldTyDesc, String kindGrId, String kindGrDesc) {
        initInstance(environment, pacId, baseId, organizationId, kindId, kindDescription, keywordId, keywordDescription, 
                    format, occurence, mandatory, length, visibleAddDoc, searchDoc, readOnly, style, unique, publish, type, mask, mapping, listType, 
                    operator, frameSearch, fldTyId, fldTyDesc, kindGrId, kindGrDesc);
    }

    public ECMKeywordTypeEntity(String environment, String pacId, String baseId, String organizationId, String kindId, String kindDescription, String keywordId, String keywordDescription, 
                              String format, String occurence, Boolean mandatory, Integer length, Boolean visibleAddDoc, Boolean searchDoc, Boolean readOnly, String style, 
                              Boolean unique, Boolean publish, String type, String mask, String mapping, String listType, String operator, Boolean frameSearch, String fldTyId,
                              String fldTyDesc, String kindGrId, String kindGrDesc, boolean canceled) {
        initInstance(environment, pacId, baseId, organizationId, kindId, kindDescription, keywordId, keywordDescription, 
                    format, occurence, mandatory, length, visibleAddDoc, searchDoc, readOnly, style, unique, publish, type, mask, mapping, listType, 
                    operator, frameSearch, fldTyId, fldTyDesc, kindGrId, kindGrDesc);
        this.canceled = canceled;
    }
    
    private void initInstance(String environment, String pacId, String baseId, String organizationId, String kindId, String kindDescription, String keywordId, String keywordDescription, 
                              String format, String occurence, Boolean mandatory, Integer length, Boolean visibleAddDoc, Boolean searchDoc, Boolean readOnly, String style, 
                              Boolean unique, Boolean publish, String type, String mask, String mapping, String listType, String operator, Boolean frameSearch, String fldTyId,
                              String fldTyDesc, String kindGrId, String kindGrDesc) {
        this.environment = environment;
        this.pacId = pacId;
        this.baseId = baseId;
        this.organizationId = organizationId;
        this.kindId = kindId;
        this.kindDescription = kindDescription;
        this.keywordId = keywordId;
        this.keywordDescription = keywordDescription;
        this.format = format;
        this.occurence = occurence;
        this.mandatory = mandatory;
        this.length = length;
        this.visibleAddDoc = visibleAddDoc;
        this.searchDoc = searchDoc;
        this.readOnly = readOnly;
        this.style = style;
        this.unique = unique;
        this.publish = publish;
        this.type = type;
        this.mask = mask;
        this.mapping = mapping;
        this.listType = listType;
        this.operator = operator;
        this.frameSearch = frameSearch;
        this.fldTyId = fldTyId;
        this.fldTyDesc = fldTyDesc;
        this.kindGrId = kindGrId;
        this.kindGrDesc = kindGrDesc;
    }

    public String getFldTyId() {
        return fldTyId;
    }

    public void setFldTyId(String fldTyId) {
        this.fldTyId = fldTyId;
    }

    public String getFldTyDesc() {
        return fldTyDesc;
    }

    public void setFldTyDesc(String fldTyDesc) {
        this.fldTyDesc = fldTyDesc;
    }

    public String getKindGrId() {
        return kindGrId;
    }

    public void setKindGrId(String kindGrId) {
        this.kindGrId = kindGrId;
    }

    public String getKindGrDesc() {
        return kindGrDesc;
    }

    public void setKindGrDesc(String kindGrDesc) {
        this.kindGrDesc = kindGrDesc;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getPacId() {
        return pacId;
    }

    public void setPacId(String pacId) {
        this.pacId = pacId;
    }

    public String getBaseId() {
        return baseId;
    }

    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getKindId() {
        return kindId;
    }

    public void setKindId(String kindId) {
        this.kindId = kindId;
    }

    public String getKindDescription() {
        return kindDescription;
    }

    public void setKindDescription(String kindDescription) {
        this.kindDescription = kindDescription;
    }

    public String getKeywordId() {
        return keywordId;
    }

    public void setKeywordId(String keywordId) {
        this.keywordId = keywordId;
    }

    public String getKeywordDescription() {
        return keywordDescription;
    }

    public void setKeywordDescription(String keywordDescription) {
        this.keywordDescription = keywordDescription;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getOccurence() {
        return occurence;
    }

    public void setOccurence(String occurence) {
        this.occurence = occurence;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Boolean getVisibleAddDoc() {
        return visibleAddDoc;
    }

    public void setVisibleAddDoc(Boolean visibleAddDoc) {
        this.visibleAddDoc = visibleAddDoc;
    }

    public Boolean getSearchDoc() {
        return searchDoc;
    }

    public void setSearchDoc(Boolean searchDoc) {
        this.searchDoc = searchDoc;
    }

    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public Boolean getUnique() {
        return unique;
    }

    public void setUnique(Boolean unique) {
        this.unique = unique;
    }

    public Boolean getPublish() {
        return publish;
    }

    public void setPublish(Boolean publish) {
        this.publish = publish;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public String getMapping() {
        return mapping;
    }

    public void setMapping(String mapping) {
        this.mapping = mapping;
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Boolean getFrameSearch() {
        return frameSearch;
    }

    public void setFrameSearch(Boolean frameSearch) {
        this.frameSearch = frameSearch;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

}
