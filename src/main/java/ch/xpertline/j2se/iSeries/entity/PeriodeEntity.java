package ch.xpertline.j2se.iSeries.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author chfa
 */
@Entity
@Table(name = "SC36FT")
@XmlRootElement
public class PeriodeEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @Column(name = "UF3AA")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer periodNumber;

    @Column(name = "UF3FA", columnDefinition = "CHAR(30)")
    private String periodDesignation;

    @Column(name = "UF39A", columnDefinition = "CHAR(10)")
    private String abbreviatedPeriodDesignation;
    
    @Column(name = "UF3GA", columnDefinition = "CHAR(1)")
    private String subperiod;    

    @Column(name = "UF3BA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodStartDate;
    
    @Column(name = "UF3CA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodEndDate;
    
    @Column(name = "UFDGA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodClosingDate;
    
    @Column(name = "UFDHA")
    private Short numberOfSubperiods;
    
    @Column(name = "UF3DA", columnDefinition = "CHAR(1)")
    private String periodClosingId;   
    
    @Column(name = "UF3EA", columnDefinition = "CHAR(1)")
    private String periodStatusCode;

    public Integer getPeriodNumber() {
        return periodNumber;
    }

    public void setPeriodNumber(Integer periodNumber) {
        this.periodNumber = periodNumber;
    }

    public String getPeriodDesignation() {
        return periodDesignation;
    }

    public void setPeriodDesignation(String periodDesignation) {
        this.periodDesignation = periodDesignation;
    }

    public String getAbbreviatedPeriodDesignation() {
        return abbreviatedPeriodDesignation;
    }

    public void setAbbreviatedPeriodDesignation(String abbreviatedPeriodDesignation) {
        this.abbreviatedPeriodDesignation = abbreviatedPeriodDesignation;
    }

    public String getSubperiod() {
        return subperiod;
    }

    public void setSubperiod(String subperiod) {
        this.subperiod = subperiod;
    }

    public Date getPeriodStartDate() {
        return periodStartDate;
    }

    public void setPeriodStartDate(Date periodStartDate) {
        this.periodStartDate = periodStartDate;
    }

    public Date getPeriodEndDate() {
        return periodEndDate;
    }

    public void setPeriodEndDate(Date periodEndDate) {
        this.periodEndDate = periodEndDate;
    }

    public Date getPeriodClosingDate() {
        return periodClosingDate;
    }

    public void setPeriodClosingDate(Date periodClosingDate) {
        this.periodClosingDate = periodClosingDate;
    }

    public Short getNumberOfSubperiods() {
        return numberOfSubperiods;
    }

    public void setNumberOfSubperiods(Short numberOfSubperiods) {
        this.numberOfSubperiods = numberOfSubperiods;
    }

    public String getPeriodClosingId() {
        return periodClosingId;
    }

    public void setPeriodClosingId(String periodClosingId) {
        this.periodClosingId = periodClosingId;
    }

    public String getPeriodStatusCode() {
        return periodStatusCode;
    }

    public void setPeriodStatusCode(String periodStatusCode) {
        this.periodStatusCode = periodStatusCode;
    }
    
    public String toString() {
        return "periodNumber=" + periodNumber;
    }
}
