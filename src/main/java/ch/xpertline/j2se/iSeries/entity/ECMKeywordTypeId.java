package ch.xpertline.j2se.iSeries.entity;

import java.io.Serializable;

/**
 *
 * @author takiet
 */
public class ECMKeywordTypeId implements Serializable {
    private String environment;
    private String pacId;
    private String baseId;
    private String kindId;
    private String keywordId;

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getPacId() {
        return pacId;
    }

    public void setPacId(String pacId) {
        this.pacId = pacId;
    }

    public String getBaseId() {
        return baseId;
    }

    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    public String getKindId() {
        return kindId;
    }

    public void setKindId(String kindId) {
        this.kindId = kindId;
    }

    public String getKeywordId() {
        return keywordId;
    }

    public void setKeywordId(String keywordId) {
        this.keywordId = keywordId;
    }
    
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        ECMKeywordTypeId castOther = (ECMKeywordTypeId) other;
        final boolean comparison1 = environment.equals(castOther.getEnvironment())
                && pacId.equals(castOther.getPacId())
                && baseId.equals(castOther.getBaseId());
        final boolean comparison2 = kindId.equals(castOther.getKindId())
                && keywordId.equals(castOther.getKeywordId());
        return comparison1 && comparison2;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + this.environment.hashCode();
        hash = hash * prime + this.pacId.hashCode();
        hash = hash * prime + this.baseId.hashCode();
        hash = hash * prime + this.kindId.hashCode();
        hash = hash * prime + this.keywordId.hashCode();
        return hash;
    }
}
