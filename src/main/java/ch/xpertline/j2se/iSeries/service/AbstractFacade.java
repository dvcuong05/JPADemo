package ch.xpertline.j2se.iSeries.service;

import ch.xpertline.component.data.technical.FindRangeCriteria;
import ch.xpertline.component.data.technical.OrderByCriteria;
import ch.xpertline.component.data.technical.OrderCriteria;
import ch.xpertline.component.data.technical.SearchByCriteria;
import ch.xpertline.component.data.technical.SearchCriteria;
import ch.xpertline.component.data.technical.SearchCriteriaComparisonExpression;
import ch.xpertline.component.data.technical.ServiceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * The AbstractFacade class generated with netbeans wizard "Create Sessions
 * Beans for Entity Classes" and extended with search methods.
 * <p>
 * Methods create, edit, remove, find, findAll, findRange, count are generated
 * by default.
 * <p>
 * The generic methods search and count with search and sort criterias where
 * added to manage additionnal criterias.
 *
 * @author tifxb
 * @param <T>
 */
public abstract class AbstractFacade<T> {

    protected ServiceContext serviceContext;
    
    private static final int BATCH_INSERT = 20;
    
    // Support Search Criteria field for embedded entity.
    // If main field is a of class A, and A contains field a1, then specify the field in Search Criteria as a.a1
    private static final String JPA_PATH_SEPARATOR_REGEX = "\\.";
    private static final String JPA_PATH_SEPARATOR = ".";
    
    private final Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    protected abstract Map<String, String> getAcceptedSearchFields();

    protected abstract Map<String, String> getAcceptedOrderFields();

    public Class<T> getEntityClass() {
        return entityClass;
    }
    
    public void create(T entity) {
        getEntityManager().persist(entity);
        getEntityManager().flush();
    }
    
    public void create(List<T> entityList) {
        if (entityList == null || entityList.isEmpty()) {
            return;
        }
        EntityManager em = getEntityManager();

        for (int i = 0; i < entityList.size(); i++) {
            T entity = entityList.get(i);
            em.persist(entity);
            if ((i + 1) % BATCH_INSERT == 0) {
                em.flush();
            }
        }
        em.flush();
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    /**
     * Leverage batch update.
     * @param entityList list of entity.
     */
    public void edit(List<T> entityList) {
        if (entityList == null || entityList.isEmpty()) {
            return;
        }
        EntityManager em = getEntityManager();
        for (T entity : entityList) {
            em.merge(entity);
        }
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    /**
     *
     * @param range
     * @return
     */
    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    /**
     * Additionnal generic method with searchBy, sortBy and range query
     * criterias
     * @param searchBy
     * @param orderBy
     * @param range
     * @return 
     */
    public List<T> search(SearchByCriteria searchBy, OrderByCriteria orderBy, FindRangeCriteria range) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<T> root = cq.from(entityClass);
        cq.select(root);

        Predicate predicate = getSearchByCriteriaPredicate(cb, cq, root, searchBy);

        if (predicate != null) {
            cq.where(predicate);
        }

        List<Order> orders = getOrderByCriteriaOrderList(cb, root, orderBy);

        if (orders != null) {
            cq.orderBy(orders);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        if (range != null) {
            q.setMaxResults(range.getTo() - range.getFrom());
            q.setFirstResult(range.getFrom());
        }

        return q.getResultList();
    }

    /**
     * Additionnal generic count method with searchBy criterias
     * @param searchBy
     * @return 
     */
    public int count(SearchByCriteria searchBy) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));

        Predicate predicate = getSearchByCriteriaPredicate(cb, cq, rt, searchBy);

        if (predicate != null) {
            cq.where(predicate);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    private String getSearchFacadeFieldName(String fieldName) {
        String facadeFieldName = null;
        for (Map.Entry<String, String> entry : getAcceptedSearchFields().entrySet()) {
            if (fieldName.compareToIgnoreCase(entry.getKey()) == 0) {
                return entry.getValue();
            }
        }
        return facadeFieldName;
    }

    private String getOrderFacadeFieldName(String fieldName) {
        String facadeFieldName = null;
        for (Map.Entry<String, String> entry : getAcceptedOrderFields().entrySet()) {
            if (fieldName.compareToIgnoreCase(entry.getKey()) == 0) {
                return entry.getValue();
            }
        }
        return facadeFieldName;
    }

    private Predicate getSearchByCriteriaPredicate(CriteriaBuilder cb, CriteriaQuery cq, Root<T> root, SearchByCriteria searchBy) {
        Predicate predicate = null;

        if (searchBy == null) {
            return null;
        }

        // Use JPA 2.0 Criteria API to add search criterias and orderBy criterias
        if (searchBy.getCriteria() != null) {
            List<Predicate> andPredicates = getSearchCriteriaPredicates(cb, cq, root, searchBy.getCriteria());
            for (Predicate andPredicate : andPredicates) {
                if (predicate == null) {
                    predicate = cb.conjunction();
                } else {
                    predicate = cb.and(cb.conjunction(), predicate);
                }

                predicate = cb.and(predicate, andPredicate);
            }
        }

        if (searchBy.getOr() != null) {
            for (SearchByCriteria orCriteria : searchBy.getOr()) {
                Predicate orPredicate = getSearchByCriteriaPredicate(cb, cq, root, orCriteria);

                if (orPredicate != null) {
                    if (predicate == null) {
                        predicate = cb.disjunction();
                    } else {
                        predicate = cb.or(cb.disjunction(), predicate);
                    }

                    predicate = cb.or(predicate, orPredicate);
                }
            }
        }

        return predicate;
    }

    /**
     * Method provided in order to override more complex criterias on the facade criterias
     */
    protected Predicate getCustomComparisonExpressionPredicate(CriteriaBuilder cb, CriteriaQuery cq, Root<T> root, String fieldName, SearchCriteria criteria) {
        return null;
    }
    
    protected Path getPath(Root<T> root, String fieldName) {
        Path path = null;

        // Get path for field of embedded entity.
        if (fieldName.contains(JPA_PATH_SEPARATOR)) {
            String[] subFieldNames = fieldName.split(JPA_PATH_SEPARATOR_REGEX);
            path = root.get(subFieldNames[0]);
            for (int i = 1; i < subFieldNames.length; i++) {
                path = path.get(subFieldNames[i]);
            }
        } else {
            path = root.get(fieldName);
        }
        return path;
    }

    private Predicate getComparisonExpressionPredicate(CriteriaBuilder cb, CriteriaQuery cq, Root<T> root, String fieldName, SearchCriteria criteria) {
        Predicate predicate;

        // Method called in order to override more complex criterias on the facade
        predicate = getCustomComparisonExpressionPredicate(cb, cq, root, fieldName, criteria);

        if (predicate != null) {
            return predicate;
        }

        Path path = getPath(root, fieldName);

        // See JPA 2.0 Criteria API for supported criterias
        if (criteria.getExpression().compareTo(SearchCriteriaComparisonExpression.EQUAL) == 0) {
            return cb.equal(path, criteria.getValue());
        }

        if (criteria.getExpression().compareTo(SearchCriteriaComparisonExpression.NOT_EQUAL) == 0) {
            return cb.notEqual(path, criteria.getValue());
        }
        
        if (criteria.getExpression().compareTo(SearchCriteriaComparisonExpression.IS_NULL) == 0) {
            return cb.isNull(path);
        }

        if (criteria.getExpression().compareTo(SearchCriteriaComparisonExpression.IS_NOT_NULL) == 0) {
            return cb.isNotNull(path);
        } 
        
        if (criteria.getExpression().compareTo(SearchCriteriaComparisonExpression.LIKE) == 0) {
            String pattern = (criteria.getValue() == null) ? "" : "%" + criteria.getValue().toString().toUpperCase() + "%";
            return cb.like(cb.upper(path), pattern);
        } 

        if (criteria.getExpression().compareTo(SearchCriteriaComparisonExpression.IN) == 0) {
            if (criteria.getValue() instanceof Collection) {
                Collection colValue = (Collection) criteria.getValue();
                // This uses a different api from the below
                return cb.and(path.in(colValue));
            }
            return cb.and(path.in(criteria.getValue()));
        }
        
        if (predicate == null) {
            throw new UnsupportedOperationException("The operation (" + criteria.getExpression().toString() + ") is not supported");
        }

        return predicate;
    }

    private List<Predicate> getSearchCriteriaPredicates(CriteriaBuilder cb, CriteriaQuery cq, Root<T> root, List<SearchCriteria> criterias) {
        List<Predicate> predicates = new ArrayList<>();

        for (SearchCriteria criteria : criterias) {
            String fieldName = getSearchFacadeFieldName(criteria.getField());

            if (fieldName != null) {
                Predicate predicate = getComparisonExpressionPredicate(cb, cq, root, fieldName, criteria);

                if (predicate != null) {
                    predicates.add(predicate);
                }
            }
        }
        return predicates;
    }

    private List<Order> getOrderByCriteriaOrderList(CriteriaBuilder cb, Root<T> root, OrderByCriteria orderBy) {
        List<Order> orders = null;

        if (orderBy == null) {
            return null;
        }

        orders = new ArrayList<>();

        for (OrderCriteria order : orderBy.getOrder()) {
            String orderFieldName = getOrderFacadeFieldName(order.getField());
            if (orderFieldName != null) {
                if (order.isDescending()) {
                    orders.add(cb.desc(getPath(root, orderFieldName)));
                } else {
                    orders.add(cb.asc(getPath(root, orderFieldName)));
                }
            }
        }
        return orders;
    }
}
