package ch.xpertline.j2se.iSeries.service;

import ch.xpertline.component.data.technical.SearchCriteria;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * TAO has an existing database, we can't change its schema.
 * @author nhvtuyen
 */

public abstract class AbstractTAOFacade<T> extends AbstractFacade<T> {
    public AbstractTAOFacade(Class<T> entityClass) {
        super(entityClass);
    }
    
    /**
     * TAO stores string as char (not varchar) so we need to override the method to get predicate of AbstractFacade.
     * @param cb
     * @param cq
     * @param criteria
     * @return 
     */
    @Override
    protected Predicate getCustomComparisonExpressionPredicate(CriteriaBuilder cb, CriteriaQuery cq, Root<T> root, String fieldName, SearchCriteria criteria) {
        Path path = getPath(root, fieldName);
        if (path != null && criteria.getValue() != null && criteria.getValue() instanceof String) {
            String trimmedValue = criteria.getValue().toString().trim();
            Expression trimmedPath = cb.trim(path);
            return cb.equal(trimmedPath, trimmedValue);
        }
        return null;
    }
}
