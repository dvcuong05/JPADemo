package ch.xpertline.j2se.iSeries.service;

import ch.xpertline.j2se.iSeries.entity.PeriodeEntity;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;

/**
 *
 * @author chfa
 */
public class PeriodeFacade extends AbstractTAOFacade<PeriodeEntity> {
    private static final String PERIOD_NUMBER = "periodNumber";

//    @PersistenceContext(unitName = "xtaoPU")
    private EntityManager em;

    public PeriodeFacade() {
        super(PeriodeEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    @Override
    public Map<String, String> getAcceptedSearchFields() {
        Map<String, String> acceptedFields = new HashMap<>();
        acceptedFields.put(PERIOD_NUMBER, PERIOD_NUMBER);
        acceptedFields.put("periodDesignation", "periodDesignation");
        
        return acceptedFields;
    }

    @Override
    public Map<String, String> getAcceptedOrderFields() {
        Map<String, String> acceptedFields = new HashMap<>();
        acceptedFields.put(PERIOD_NUMBER, PERIOD_NUMBER);
        return acceptedFields;
    }
}
