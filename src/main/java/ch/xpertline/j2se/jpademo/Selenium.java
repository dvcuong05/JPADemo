
package ch.xpertline.j2se.jpademo;


import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author dvcuong
 */
public class Selenium {
    public static WebDriver driver;
    public static void main(String[] args){
        try{
            System.out.println("launching chrome browser");
            System.setProperty("webdriver.chrome.driver", "D:\\Temporary\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
	driver.navigate().to("D:\\Temporary\\chromedriver_win32\\test.html");
        
        // Added a wait to make you notice the difference.
        Thread.sleep(3000);
        //driver.findElement(By.id("vncud-file")).click();
        
        WebElement fileInput = driver.findElement(By.id("vncud-file"));
        fileInput.sendKeys("d:\\QuickLinks\\Test data\\png.png");
        
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);    
        driver.manage().window().maximize();
        //To open a new tab         
        Robot r = new Robot();                          
        r.keyPress(KeyEvent.VK_CONTROL); 
        r.keyPress(KeyEvent.VK_T); 
        r.keyRelease(KeyEvent.VK_CONTROL); 
        r.keyRelease(KeyEvent.VK_T);    
        //To switch to the new tab
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        //To navigate to new link/URL in 2nd new tab
        driver.get("http://facebook.com");

        }catch(Exception e){
             System.out.println("vncud excetiption:"+e.getMessage());
        }     
    }
}
