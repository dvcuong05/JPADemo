package ch.xpertline.j2se.jpademo;

import com.google.gson.JsonSyntaxException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.Consts;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A helper class for executing HTTP requests and processing HTTP responses.
 *
 * @author vnxvd
 */
public final class HttpUtils {

    public enum HttpMethods {

        GET, POST, PUT, DELETE
    }

    public static final int HTTP_STATUS_CODE_OK = 200;
    public static final int HTTP_STATUS_CODE_CREATED = 201;
    public static final int HTTP_STATUS_CODE_NO_CONTENT = 204;
    public static final int HTTP_STATUS_CODE_UNAUTHORIZED = 401;
    public static final int HTTP_STATUS_CODE_FORBIDDEN = 403;
    public static final int HTTP_STATUS_CODE_NOT_FOUND = 404;

    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String ACCEPT_HEADER = "Accept";
    private static final String UTF8 = "utf-8";
    private static final String ISO_8859_1 = "ISO-8859-1";

    private static final Logger log = LoggerFactory.getLogger(HttpUtils.class);

    /**
     * @see http://hc.apache.org/httpclient-3.x/performance.html. Generally it
     * is recommended to have a single instance of HttpClient per communication
     * component or even per application
     */
    private static final CloseableHttpClient CLIENT;

    static {
        CloseableHttpClient temp = null;
        KeyStore trustStore;
        File trustFile = null;
        String trustPath = "";

        // Load truststore file
        try {
            String trustStorePassword = HttpContext.getSSLClientTrustStorePassword();
            trustPath = HttpContext.getSSLClientTrustStoreFile();
            trustFile = new File(trustPath);

            trustStore = KeyStore.getInstance(HttpContext.getSSLClientTrustStoreType());

            if (trustFile.exists()) {
                log.debug("Loading http ssl client trust store file : " + trustFile.getAbsolutePath());
                trustStore.load(new FileInputStream(trustFile), trustStorePassword.toCharArray());
            } else {
                // Create empty truststore
                log.warn(String.format("Http ssl client trust store file (%s) was not found, loading empty trustore. Please verify (ch.xpertline.component.configuration.HttpSSLClientTrustStoreFile) configuration variables.", trustFile.getAbsolutePath()));
                trustStore.load(null, trustStorePassword.toCharArray());
            }
        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException e) {
            if (trustFile != null) {
                log.info("Loading http ssl client trust store file : " + trustFile.getAbsolutePath());
            } else {
                log.info("Loading http ssl client trust store with configuration path : " + trustPath);
            }
            log.warn("Http ssl client trust store cannot be loaded. Please verify (ch.xpertline.component.configuration.HttpSSL*) configuration variables. ", e);
            trustStore = null;
        }

        try {
            SSLContextBuilder builder = new SSLContextBuilder();
            if (trustStore != null) {
                builder.loadTrustMaterial(trustStore, new TrustSelfSignedStrategy());
            }

            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build(),
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", new PlainConnectionSocketFactory())
                    .register("https", sslsf)
                    .build();

            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
            // Increase max total connection to 200
            cm.setMaxTotal(HttpContext.getHttpConnectionMaxTotal());
            log.debug("The Http Connection Max Total is " + cm.getMaxTotal());
            // Increase default max connection per route to 20
            cm.setDefaultMaxPerRoute(HttpContext.getHttpConnectionMaxPerRoute());
            log.debug("The Http Connection Max Per Route is " + cm.getDefaultMaxPerRoute());
            temp = HttpClients.custom().setSSLSocketFactory(sslsf).setConnectionManager(cm).build();
        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
            log.error("Error inializing closeable http client instance ", e);
        }
        CLIENT = temp;
    }

    private HttpUtils() {

    }

    /**
     * Executes an HTTP request to the server with support basic authentication
     * and returns original HttpResponse and exceptions. HttpResponse must be
     * closed connection.
     *
     * @param method to use : GET, POST, PUT, DELETE
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param entityData is the entity object to send with post or put methods
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @param accept Response content type (ie : application/json,
     * application/xml,etc...)
     * @param username to authenticate with basic authentication, can be null
     * (no authentication)
     * @param password to authenticate with basic authentication, can be null
     * (no authentication)
     * @return HttpResponse
     * @throws java.net.URISyntaxException
     * @throws org.apache.http.auth.AuthenticationException
     * @throws java.io.IOException
     */
    public static HttpResponse execute(HttpMethods method, String uri, Map<String, Object> parameters, Object entityData, String contentType, String accept,
            String username, String password) throws URISyntaxException, IOException, AuthenticationException {
        URI url = buildURI(uri, parameters);
        HttpRequestBase request = buildRequest(method, url, entityData, contentType);
        if (request == null) {
            return null;
        }
        if (username != null && password != null) {
            UsernamePasswordCredentials creds = new UsernamePasswordCredentials(username, password);
            request.addHeader(new BasicScheme().authenticate(creds, request, null));
        }
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(contentType)) {
            request.setHeader(CONTENT_TYPE_HEADER, contentType);
        }
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(accept)) {
            request.setHeader(ACCEPT_HEADER, accept);
        }
        return CLIENT.execute(request);
    }

    /**
     * Executes an HTTP GET request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param contentType Request & Response content type (ie :
     * application/json, application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeGet(String uri, Map<String, Object> parameters, String contentType)
            throws HttpException {
        HttpResponse response = executeMethod(HttpMethods.GET, uri, parameters, null, contentType, contentType);
        return buildResponseString(response);
    }

    /**
     * Executes an HTTP GET request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @param accept Response content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeGet(String uri, Map<String, Object> parameters, String contentType,
            String accept) throws HttpException {
        HttpResponse response = executeMethod(HttpMethods.GET, uri, parameters, null, contentType, accept);
        return buildResponseString(response);
    }

    /**
     * Executes an HTTP GET request to the server with basic authentication.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @param accept Response content type (ie : application/json,
     * application/xml,etc...)
     * @param username
     * @param password
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeGet(String uri, Map<String, Object> parameters, String contentType, String accept, String username, String password) throws HttpException {
        HttpResponse response = executeMethod(HttpMethods.GET, uri, parameters, null, contentType, accept, username, password);
        return buildResponseString(response);
    }

    /**
     * Execute an HTTP request to the server with basic authentication and custom HTTP Header.
     *
     * @param method
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param headerParameters
     * @param data
     * @param contentType Request content type (ie : application/json,
     * application/xml, etc...)
     * @param accept Response content type (ie : application/json,
     * application/xml, etc...)
     * @param username
     * @param password
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeMethod(HttpMethods method, String uri, Map<String, Object> parameters, Map<String, String> headerParameters, Object data, String contentType, String accept, String username, String password) throws HttpException {
        HttpEntity entity = buildStringEntity(data, contentType);
        HttpResponse response = executeMethod(method, uri, parameters, headerParameters, entity, contentType, accept, username, password);
        return buildResponseString(response);
    }

    /**
     * Executes an HTTP GET request to the server with basic authentication.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param headerParameters
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @param accept Response content type (ie : application/json,
     * application/xml,etc...)
     * @param username
     * @param password
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeGet(String uri, Map<String, Object> parameters, Map<String, String> headerParameters, String contentType, String accept, String username, String password) throws HttpException {
        HttpResponse response = executeMethod(HttpMethods.GET, uri, parameters, headerParameters, null, contentType, accept, username, password);
        return buildResponseString(response);
    }

    /**
     * Executes an muti-part HTTP GET request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param contentType Request & Response content type (ie :
     * application/json, application/xml,etc...)
     * @return Server response in InputStream
     * @throws HttpException
     */
    public static InputStream executeMultiPartGet(String uri, Map<String, Object> parameters, String contentType)
            throws HttpException {
        HttpResponse response = executeMethod(HttpMethods.GET, uri, parameters, null, contentType, contentType);
        return getInputStream(response);
    }

    /**
     * Executes an HTTP GET request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @param accept Response content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in InputStream
     * @throws HttpException
     */
    public static InputStream executeMultiPartGet(String uri, Map<String, Object> parameters,
            String contentType, String accept) throws HttpException {
        HttpResponse response = executeMethod(HttpMethods.GET, uri, parameters, null, contentType, accept);
        return getInputStream(response);
    }

    /**
     * Execute POST method for Ivy service with follow redirect.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param params
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeIvyPost(String uri, Map<String, Object> parameters, List<NameValuePair> params,
            String contentType) throws HttpException {
        String encodedData = URLEncodedUtils.format(params, ISO_8859_1);
        HttpEntity entity = buildStringEntity(encodedData, contentType);
        CloseableHttpClient httpClient = HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build();
        HttpResponse postResponse = executeMethod(httpClient, HttpMethods.POST, uri, parameters, entity, contentType, null);
        return buildResponseString(postResponse, ISO_8859_1);
    }

    /**
     * Executes an HTTP POST request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param data Request data ( ie : string, object, etc...)
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executePost(String uri, Map<String, Object> parameters, Object data, String contentType)
            throws HttpException {
        HttpEntity entity = buildStringEntity(data, contentType);
        HttpResponse response = executeMethod(HttpMethods.POST, uri, parameters, entity, contentType, null);
        return buildResponseString(response);
    }

    /**
     * Executes an HTTP POST request to the server and return HTTPResponse
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param data Request data ( ie : string, object, etc...)
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @param accept
     * @param username
     * @param password
     * @return Server response in string
     * @throws HttpException
     */
    public static HttpResponse executePostAndReturnResponse(String uri, Map<String, Object> parameters, Object data, String contentType, String accept,
            String username, String password) throws HttpException {
        HttpEntity entity = buildStringEntity(data, contentType);
        return executeMethod(HttpMethods.POST, uri, parameters, entity, contentType, accept, username, password);
    }

    /**
     * Executes an HTTP POST request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param data Request data ( ie : string, object, etc...)
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @param accept Response content type (ie : application/json,
     * application/xml,etc...) * @param accept Response content type (ie :
     * application/json, application/xml,etc...)
     * @param username to authenticate with basic authentication, can be null
     * (no authentication)
     * @param password to authenticate with basic authentication, can be null
     * (no authentication)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executePost(String uri, Map<String, Object> parameters, Object data, String contentType, String accept,
            String username, String password) throws HttpException {
        HttpEntity entity = buildStringEntity(data, contentType);
        HttpResponse response = executeMethod(HttpMethods.POST, uri, parameters, entity, contentType, accept, username, password);
        return buildResponseString(response);
    }

    /**
     * Executes an HTTP POST request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param data Request data ( ie : string, object, etc...)
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @param accept Response content type (ie : application/json,
     * application/xml,etc...) * @param accept Response content type (ie :
     * application/json, application/xml,etc...)
     * @param username to authenticate with basic authentication, can be null
     * (no authentication)
     * @param password to authenticate with basic authentication, can be null
     * (no authentication)
     * @param action to add action key for SOAP
     * @return Server response in string
     * @throws HttpException
     */
    public static String executePost(String uri, Map<String, Object> parameters, Object data, String contentType, String accept,
            String username, String password, String action) throws HttpException {
        HttpEntity entity = buildStringEntity(data, contentType);
        HttpResponse response = executeMethod(HttpMethods.POST, uri, parameters, entity, contentType, accept, username, password, action);
        return buildResponseString(response);
    }

    /**
     * Executes an HTTP POST request to the server. Use in case when we search
     * with long query parameters.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param data Request data ( ie : string, object, etc...)
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executePostSearch(String uri, Map<String, Object> parameters, Object data, String contentType) throws HttpException {
        URI url = buildURI(uri, parameters);
        HttpEntity entity = buildStringEntity(url.getRawQuery(), contentType);
        HttpResponse response = executeMethod(HttpMethods.POST, uri, null, entity, contentType, null);
        return buildResponseString(response);
    }

    /**
     * Executes an HTTP POST request to the server. Use in case when we search
     * with long query parameters.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param data Request data ( ie : string, object, etc...)
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @param username to authenticate with basic authentication, can be null
     * (no authentication)
     * @param password to authenticate with basic authentication, can be null
     * (no authentication)
     * @return Server response in string
     * @throws org.apache.http.auth.AuthenticationException
     * @throws java.io.IOException
     * @throws ch.xpertline.component.exception.HttpException
     */
    public static String executePostSearch(String uri, Map<String, Object> parameters, Object data, String contentType,
            String username, String password) throws AuthenticationException, IOException, HttpException {
        URI url = buildURI(uri, parameters);
        HttpEntity entity = buildStringEntity(url.getRawQuery(), contentType);
        HttpResponse response = executeMethod(HttpMethods.POST, uri, null, entity, contentType, null, username, password);
        return buildResponseString(response);
    }

    /**
     * Executes a multi-part HTTP POST request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param in InputStream
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeMultiPartPost(String uri, Map<String, Object> parameters, InputStream in,
            String contentType) throws HttpException {
        HttpEntity entity = new InputStreamEntity(in, -1);
        HttpResponse response = executeMethod(HttpMethods.POST, uri, parameters, entity, contentType, null);
        return buildResponseString(response);
    }

    /**
     * Executes a multi-part HTTP POST request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param file Physical file
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeMultiPartPost(String uri, Map<String, Object> parameters, File file,
            String contentType) throws HttpException {
        FileEntity entity = new FileEntity(file);
        entity.setContentType(contentType);
        HttpResponse response = executeMethod(HttpMethods.POST, uri, parameters, entity, contentType, null);
        return buildResponseString(response);
    }

    /**
     * Executes a multi-part HTTP POST request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param data List of bytes
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeMultiPartPost(String uri, Map<String, Object> parameters, byte[] data,
            String contentType) throws HttpException {
        HttpEntity entity = new ByteArrayEntity(data);
        HttpResponse response = executeMethod(HttpMethods.POST, uri, parameters, entity, contentType, null);
        return buildResponseString(response);
    }

    /**
     * Executes an HTTP DELETE request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeDelete(String uri, Map<String, Object> parameters, String contentType)
            throws HttpException {
        HttpResponse response = executeMethod(HttpMethods.DELETE, uri, parameters, null, contentType, null);
        return buildResponseString(response);
    }

    /**
     * Executes an HTTP DELETE request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @param username
     * @param password
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeDelete(String uri, Map<String, Object> parameters, String contentType, String username, String password)
            throws HttpException {
        HttpResponse response = executeMethod(HttpMethods.DELETE, uri, parameters, null, contentType, null, username, password);
        return buildResponseString(response);
    }

    public static String executeDelete(String uri, Map<String, Object> parameters, Object data, String contentType, String username, String password)
            throws HttpException {
        StringEntity entity = buildStringEntity(data, contentType);
        HttpResponse response = executeMethod(HttpMethods.DELETE, uri, parameters, entity, contentType, null, username, password);
        return buildResponseString(response);
    }

    /**
     * Executes an HTTP PUT request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param data Request data ( ie : string, object, etc...)
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executePut(String uri, Map<String, Object> parameters, Object data, String contentType)
            throws HttpException {
        StringEntity entity = buildStringEntity(data, contentType);
        HttpResponse response = executeMethod(HttpMethods.PUT, uri, parameters, entity, contentType, null);
        return buildResponseString(response);
    }

    public static String executePut(String uri, Map<String, Object> parameters, Object data, String contentType, String accept,
            String username, String password) throws HttpException {
        HttpEntity entity = buildStringEntity(data, contentType);
        HttpResponse response = executeMethod(HttpMethods.PUT, uri, parameters, entity, contentType, accept, username, password);
        return buildResponseString(response);
    }

    /**
     * Executes a multi-part HTTP PUT request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param data List of bytes
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeMultiPartPut(String uri, Map<String, Object> parameters, byte[] data,
            String contentType) throws HttpException {
        HttpEntity entity = new ByteArrayEntity(data);
        HttpResponse response = executeMethod(HttpMethods.PUT, uri, parameters, entity, contentType, null);
        return buildResponseString(response);
    }

    /**
     * Executes a multi-part HTTP PUT request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param in InputStream
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeMultiPartPut(String uri, Map<String, Object> parameters, InputStream in,
            String contentType) throws HttpException {
        HttpEntity entity = new InputStreamEntity(in, 0);
        HttpResponse response = executeMethod(HttpMethods.PUT, uri, parameters, entity, contentType, null);
        return buildResponseString(response);
    }

    /**
     * Executes a multi-part HTTP PUT request to the server.
     *
     * @param uri Server URI
     * @param parameters A map (key: name of parameter, value: value of
     * parameter) contains the list of parameters of the request.
     * @param file Physical file
     * @param contentType Request content type (ie : application/json,
     * application/xml,etc...)
     * @return Server response in string
     * @throws HttpException
     */
    public static String executeMultiPartPut(String uri, Map<String, Object> parameters, File file,
            String contentType) throws HttpException {
        FileEntity entity = new FileEntity(file);
        entity.setContentType(contentType);
        HttpResponse response = executeMethod(HttpMethods.PUT, uri, parameters, entity, contentType, null);
        return buildResponseString(response);
    }

    /**
     * Validates the HTTP execution result based on the status code. If the
     * status code value is >= 400 and <= 599 (client & server errors), an
     * exception will be thrown). @param response HTTP execution response
     * @throws HttpException
     */
    private static void validateResponse(HttpResponse response) throws HttpException {
        int statusCode = response.getStatusLine().getStatusCode();
        // For more information, please visit this link : http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
        if (statusCode >= 400 && statusCode <= 599) {
            String s = buildResponseString(response);
//            ResultStatus status = null;
//            try {
//                status = ResultStatusBuilderUtils.build(JsonUtils.fromJson(s, EIResultStatus.class));
//            } catch (ComponentException e) {
//                log.debug("validateResponse: there was an exception", e);
//            }
            // When we call an external REST, status may be not null but code is blank/null
           // throw (status != null && StringUtils.isNotBlank(status.getCode())) ? new HttpException(statusCode, status) : new HttpException(statusCode, s);
        }
    }

    private static StringEntity buildStringEntity(Object data, String contentType) {
        StringEntity entity = new StringEntity(String.valueOf(data), Consts.UTF_8);
        entity.setContentType(contentType);
        return entity;
    }

    private static HttpRequestBase buildRequest(HttpMethods method, String uri, Map<String, Object> parameters,
            HttpEntity entity, String contentType, String accept) {
        URI url = buildURI(uri, parameters);
        if (url == null) {
            return null;
        }
        HttpRequestBase request = buildRequest(method, url, entity);
        if (request == null) {
            return null;
        }
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(contentType)) {
            request.setHeader(CONTENT_TYPE_HEADER, contentType);
        }
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(accept)) {
            request.setHeader(ACCEPT_HEADER, accept);
        }
        return request;
    }

    private static HttpRequestBase buildRequest(HttpMethods method, String uri, Map<String, Object> parameters, Map<String, String> headerParameters,
            HttpEntity entity) {
        URI url = buildURI(uri, parameters);
        if (url == null) {
            return null;
        }
        HttpRequestBase request = buildRequest(method, url, entity);
        if (request == null) {
            return null;
        }
        if (headerParameters != null) {
            Set<String> names = headerParameters.keySet();
            for (String name : names) {
                request.setHeader(name, headerParameters.get(name));
            }
        }
        return request;
    }

    private static URI buildURI(String uri, Map<String, Object> parameters) {
        URIBuilder builder = new URIBuilder();
        builder.setPath(uri);
        if (parameters != null) {
            Set<String> names = parameters.keySet();
            for (String name : names) {
                builder.setParameter(name, parameters.get(name).toString());
            }
        }
        URI url = null;
        try {
            url = builder.build();
        } catch (URISyntaxException ex) {
            log.debug("buildURI: there was an exception", ex);
        }
        return url;
    }

    private static HttpResponse executeMethod(HttpMethods method, String uri, Map<String, Object> parameters,
            HttpEntity entity, String contentType, String accept) throws HttpException {
        HttpRequestBase request = buildRequest(method, uri, parameters, entity, contentType, accept);
        if (request == null) {
            return null;
        }
        try {
            HttpResponse response = CLIENT.execute(request);
            validateResponse(response);
            return response;
        } catch (IOException e) {
            // Throw Bad Request Exception
            throw new HttpException();
        }
    }

    private static HttpResponse executeMethod(HttpMethods method, String uri, Map<String, Object> parameters,
            HttpEntity entity, String contentType, String accept, String username, String password) throws HttpException {
        HttpRequestBase request = buildRequest(method, uri, parameters, entity, contentType, accept);
        if (request == null) {
            return null;
        }
        try {
            UsernamePasswordCredentials creds = new UsernamePasswordCredentials(username, password);
            request.addHeader(new BasicScheme().authenticate(creds, request, null));
        } catch (AuthenticationException ae) {
            throw new HttpException();
        }

        try {
            HttpResponse response = CLIENT.execute(request);

            validateResponse(response);
            return response;
        } catch (IOException e) {
            // Throw Bad Request Exception
            throw new HttpException(e.getMessage());
        }
    }

    private static HttpResponse executeMethod(HttpMethods method, String uri, Map<String, Object> parameters,
            HttpEntity entity, String contentType, String accept, String username, String password, String action) throws HttpException {
        HttpRequestBase request = buildRequest(method, uri, parameters, entity, contentType, accept);
        if (request == null) {
            return null;
        }
        try {
            UsernamePasswordCredentials creds = new UsernamePasswordCredentials(username, password);
            request.addHeader(new BasicScheme().authenticate(creds, request, null));
            if (action != null && !action.isEmpty()) {
                request.addHeader("SOAPAction", action);
            }
        } catch (AuthenticationException ae) {
            throw new HttpException(ae.getMessage());
        }

        try {
            HttpResponse response = CLIENT.execute(request);

            validateResponse(response);
            return response;
        } catch (IOException e) {
            // Throw Bad Request Exception
            throw new HttpException(e.getMessage());
        }
    }

    /**
     * Execute method with specific HttpClient
     *
     * @param httpClient
     * @param method
     * @param uri
     * @param parameters
     * @param entity
     * @param contentType
     * @param accept
     * @return
     * @throws HttpException
     */
    private static HttpResponse executeMethod(HttpClient httpClient, HttpMethods method, String uri, Map<String, Object> parameters,
            HttpEntity entity, String contentType, String accept) throws HttpException {
        HttpRequestBase request = buildRequest(method, uri, parameters, entity, contentType, accept);
        if (request == null) {
            return null;
        }
        try {
            HttpResponse response = httpClient.execute(request);
            validateResponse(response);
            return response;
        } catch (IOException e) {
            // Throw Bad Request Exception
            throw new HttpException(e.getMessage());
        }
    }

    private static HttpResponse executeMethod(HttpMethods method, String uri, Map<String, Object> parameters, Map<String, String> headerParameters,
            HttpEntity entity, String contentType, String accept, String username, String password) throws HttpException {
        if (headerParameters == null) {
            headerParameters = new HashMap<>();
        }
        headerParameters.put(CONTENT_TYPE_HEADER, contentType);
        headerParameters.put(ACCEPT_HEADER, accept);

        HttpRequestBase request = buildRequest(method, uri, parameters, headerParameters, entity);
        if (request == null) {
            return null;
        }
        if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)) {
            try {
                UsernamePasswordCredentials creds = new UsernamePasswordCredentials(username, password);
                request.addHeader(new BasicScheme().authenticate(creds, request, null));
            } catch (AuthenticationException ae) {
                throw new HttpException(ae.getMessage());
            }
        }

        try {
            HttpResponse response = CLIENT.execute(request);

            validateResponse(response);
            return response;
        } catch (IOException e) {
            // Throw Bad Request Exception
            throw new HttpException(e.getMessage());
        }
    }

    private static String buildResponseString(HttpResponse response) {
        return buildResponseString(response, UTF8);
    }

    private static String buildResponseString(HttpResponse response, String encoding) {
        try {
            return response.getEntity() != null ? EntityUtils.toString(response.getEntity(), encoding) : null;
        } catch (IOException ex) {
            log.debug("buildResponseString: there was an exception", ex);
        } finally {
            closeHttpResponse(response);
        }
        return null;
    }

    private static HttpRequestBase buildRequest(HttpMethods method, URI url, HttpEntity entity) {
        HttpRequestBase request = null;
        if (HttpMethods.GET == method) {
            request = new HttpGet(url);
        } else if (HttpMethods.POST == method) {
            request = new HttpPost(url);
            ((HttpPost) request).setEntity(entity);
        } else if (HttpMethods.PUT == method) {
            request = new HttpPut(url);
            ((HttpPut) request).setEntity(entity);
        } else if (HttpMethods.DELETE == method) {
            request = new HttpDelete(url);
        }
        return request;
    }

    private static HttpRequestBase buildRequest(HttpMethods method, URI url, Object entityData, String contentType) {
        HttpRequestBase request = null;
        if (HttpMethods.GET == method) {
            request = new HttpGet(url);
        } else if (HttpMethods.POST == method) {
            request = new HttpPost(url);
            HttpEntity entity = buildStringEntity(entityData, contentType);
            ((HttpPost) request).setEntity(entity);
        } else if (HttpMethods.PUT == method) {
            request = new HttpPut(url);
            HttpEntity entity = buildStringEntity(entityData, contentType);
            ((HttpPut) request).setEntity(entity);
        } else if (HttpMethods.DELETE == method) {
            request = new HttpDelete(url);
        }
        return request;
    }

    private static InputStream getInputStream(HttpResponse response) throws IllegalStateException {
        try {
            return response.getEntity() != null ? response.getEntity().getContent() : null;
        } catch (IOException e) {
            log.debug("getInputStream: there was an exception", e);
        } finally {
            closeHttpResponse(response);
        }
        return null;
    }

    public static void closeHttpResponse(HttpResponse response) {
        if (response instanceof CloseableHttpResponse) {
            try {
                ((CloseableHttpResponse) response).close();
            } catch (IOException ex) {
                log.error("closeHttpResponse: {0}", ex);
            }
        }
    }
}
