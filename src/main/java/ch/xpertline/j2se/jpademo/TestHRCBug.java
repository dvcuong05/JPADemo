package ch.xpertline.j2se.jpademo;

import ch.xpertline.j2se.jpademo.entity.HrcPerson;
import ch.xpertline.j2se.jpademo.entity.HrcPersonExternal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Bug: Multiple representations of the same entity.
 * @author nhvtuyen
 */
public class TestHRCBug {
    public static void main(String[] args) {
        // http://docs.jboss.org/hibernate/orm/4.1/manual/en-US/html/ch04.html#persistent-classes-equalshashcode
        EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("jpa-example");
        EntityManager em = emFactory.createEntityManager();
        em.getTransaction().begin();
        System.out.println("@@@ new instance");
        HrcPerson person = new HrcPerson();
        HrcPersonExternal external = new HrcPersonExternal();
        external.setName("name");
        external.setExternalRef("external ref");
        external.setObjectId(person);
        List<HrcPersonExternal> externalList = new ArrayList<>();
        externalList.add(external);
        person.setExternalRefs(externalList);
        System.out.println("@@@ Save to make attached instances");
//        em.persist(person);
        em.merge(person);
        em.detach(person);
        em.detach(external);
        System.out.println("@@@ detached entities already, attaching again");
        em.merge(person);
        em.getTransaction().commit();
        em.close();
        emFactory.close();
        
        
        
        System.out.println("@@@ EM closed, all become detached");
        emFactory = Persistence.createEntityManagerFactory("jpa-example");
        em = emFactory.createEntityManager();
        em.getTransaction().begin();
        System.out.println("@@@ Trying to save detached instances");
        em.merge(person);
//        em.merge(external);
        em.getTransaction().commit();
        em.close();
        emFactory.close();
    }
}
