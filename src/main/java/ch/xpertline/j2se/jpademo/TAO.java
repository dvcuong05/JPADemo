package ch.xpertline.j2se.jpademo;

import ch.xpertline.j2se.xtao.entity.ElementTaxationEntity;
import ch.xpertline.j2se.xtao.entity.ElementTaxationRelationEntity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author nhvtuyen
 */
public class TAO {
    public static void main(String[] args) {
        System.out.println("Start");
        EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("jpa-example");
        EntityManager em = emFactory.createEntityManager();
        em.getTransaction().begin();
        
        Query q = em.createQuery("FROM ElementTaxationEntity as t1, ElementTaxationRelationEntity as t2 WHERE t1.id = t2.id");
        Object o = q.getSingleResult();
        if (o instanceof Object[]) {
            Object[] result = (Object[])o;
            ElementTaxationEntity entity1 = (ElementTaxationEntity)result[0];
            ElementTaxationRelationEntity entity2 = (ElementTaxationRelationEntity)result[1];
            System.out.println("@@@@@@@@@@@ entity 1 = " + entity1);
            System.out.println("@@@@@@@@@@@ entity 2 = " + entity2);
            
        }
        System.out.println("@@@@@@@@@ o = " + o);
        em.getTransaction().commit();
        em.close();
        emFactory.close();
        System.out.println("Finish");
    }
}
