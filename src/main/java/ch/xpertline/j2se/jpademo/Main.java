package ch.xpertline.j2se.jpademo;

import ch.xpertline.j2se.iSeries.entity.ECMKeywordTypeEntity;
import ch.xpertline.j2se.jpademo.entity.HrcBranch;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rits.cloning.Cloner;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author nhvtuyen
 */
public class Main {

    public static void main(String[] args) throws SQLException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Cloner cloner = new Cloner();
        
        EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("sql");
        EntityManager em = emFactory.createEntityManager();
        em.getTransaction().begin();

        System.out.println("TESS:"+ countBusinessRule(em, "XECssfsM","Document", ""));
        
        em.getTransaction().commit();
        em.close();
        emFactory.close();
    }
    
    public List getExtendedDocumentsInfo(String environmentName, int maxRow, boolean isAS400, boolean isLongNameSupport) {
        String as400FetchRow = "FETCH FIRST "+maxRow+" ROWS ONLY";
        String sqlServerFetchRow = "TOP "+maxRow;
        String queryStr = "SELECT {sqlServerFetchRow} CAST(OP5OA AS VARCHAR(12)) OP5OA, "
                + " CAST(OP68A AS VARCHAR(30)) OP68A, "
                + "CAST(OP64A AS VARCHAR(256)) OP64A,"
                + "CAST(OP65A AS VARCHAR(256)) OP65A,"
                + "CAST(OP6AA AS VARCHAR(30)) OP6AA,"
                + "CAST(OP6BA AS VARCHAR(30)) OP6BA,"
                + "CAST(OP6XA AS VARCHAR(256)) OP6XA,"
                + "CAST(KEYID AS VARCHAR(100)) KEYID "
                + "FROM OP8WT WHERE NOT EXISTS (SELECT 1 "
                +        " FROM ecm_migration m "
                +        " WHERE m.documentId = OP8WT.OP5OA) {as400FetchRow}";
        if (isLongNameSupport) {
            queryStr = "SELECT {sqlServerFetchRow} CAST(OP5OA AS VARCHAR(12)) OP5OA,"
                    + "CAST(OP68A AS VARCHAR(30)) OP68A,"
                    + "CAST(OP64A AS VARCHAR(256)) OP64A,"
                    + "CAST(OP65A AS VARCHAR(256)) OP65A,"
                    + "CAST(OP6AA AS VARCHAR(30)) OP6AA,"
                    + "CAST(OP6BA AS VARCHAR(30)) OP6BA,"
                    + "CAST(OP6XA AS VARCHAR(256)) OP6XA,"
                    + "CAST(KEYID AS VARCHAR(100)) KEYID,"
                    + "CAST(OP1K9A AS VARCHAR(1024)) OP1K9A "
                    + "FROM OP8WT WHERE NOT EXISTS (SELECT 1 "
                    +        " FROM ecm_migration m "
                    +        " WHERE m.documentId = OP8WT.OP5OA) {as400FetchRow}";
        }
        if(isAS400){
            queryStr = queryStr.replace("{as400FetchRow}", as400FetchRow);
            queryStr = queryStr.replace("{sqlServerFetchRow}", "");
        }else{
            queryStr = queryStr.replace("{as400FetchRow}", "");
            queryStr = queryStr.replace("{sqlServerFetchRow}", sqlServerFetchRow);
        }
        Query query = em.createNativeQuery(queryStr);
        return query.getResultList();
    }
    
     public static int countBusinessRule(EntityManager em, String softwareCode, String objectType, String objectSubType) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT count(distinct(bRule.id)) ");
        sb.append("FROM framework_security_business_data_rule AS bRule ");
        sb.append("LEFT JOIN framework_security_business_data_expression AS bRuleExpression  ");
        sb.append("    ON bRuleExpression.businessDataRuleId = brule.id  ");
        sb.append("LEFT JOIN framework_security_business_data    AS bdata  ");
        sb.append("    ON bdata.id = bRuleExpression.businessDataId  ");
        sb.append("WHERE  ");
        sb.append("bdata.softwareCode = :softwareCode AND bdata.objectType = :objectType AND bdata.objectSubType = :objectSubType ");;
        
        Query query = em.createNativeQuery(sb.toString());
        query.setParameter("softwareCode", softwareCode);
        query.setParameter("objectType", objectType);
        query.setParameter("objectSubType", objectSubType);
        return Integer.parseInt(query.getSingleResult().toString());
    }
     
    private static String getQueryStr() {
        String sql =  "SELECT  KIKW.env, " +
                        "      KIKW.pacId, " +
                        "      KIKW.baseId, " +
                        "      KIKW.orgId, " +
                        "      KIKW.kindId, " +
                        "      KIKW.kindDesc, " +
                        "      KIKW.kwId, " +
                        "      p_dataty.formatted_json kwDesc, " +
                        "      KIKW.format, " +
                        "      KIKW.occurence, " +
                        "      KIKW.mandatory, " +
                        "      KIKW.length, " +
                        "      KIKW.visiAddDoc, " +
                        "      KIKW.searchDoc, " +
                        "      KIKW.readOnly, " +
                        "      KIKW.style, " +
                        "      KIKW.kwunique, " +
                        "      KIKW.publish, " +
                        "      KIKW.type, " +
                        "      KIKW.mask, " +
                        "      KIKW.mapping, " +
                        "      KIKW.lsttype, " +
                        "      KIKW.operator, " +
                        "      KIKW.frameSearc, " +
                        "      KIKW.fldTyId, " +
                        "      KIKW.fldTyDesc, " +
                        "      KIKW.kindGrId, " +
                        "      KIKW.kindGrDesc," +
                        "      CASE WHEN p_dataty.radiated = 1" +
                        "      THEN 'True' ELSE 'False' END AS canceled   " +
                        " FROM ECM_KIKW KIKW " +
                        " LEFT JOIN ecm_param p_dataty " +
                        " ON p_dataty.param_id = KIKW.kwId AND p_dataty.param_type=:DATATYPE ";
        return sql;
    }
    
    /***
     * Method supports to format raw result returned from query
     * @param resultList
     * @return 
     */
    private static List<ECMKeywordTypeEntity> formatResult(List resultList) {
        List entities = (List) resultList.stream().map(row
                -> {
            Object[] objs = (Object[]) row;
            ECMKeywordTypeEntity keywordTypeEntity = new ECMKeywordTypeEntity(
                    convertToString(objs[0]),
                    convertToString(objs[1]),
                    convertToString(objs[2]),
                    convertToString(objs[3]),
                    convertToString(objs[4]),
                    convertToString(objs[5]),
                    convertToString(objs[6]),
                    convertToString(objs[7]),
                    convertToString(objs[8]),
                    convertToString(objs[9]),
                    (Boolean)objs[10],
                    (Integer)objs[11],
                    (Boolean)objs[12],
                    (Boolean)objs[13],
                    (Boolean)objs[14],
                    convertToString(objs[15]),
                    (Boolean)objs[16],
                    (Boolean)objs[17],
                    convertToString(objs[18]),
                    convertToString(objs[19]),
                    convertToString(objs[20]),
                    convertToString(objs[21]),
                    convertToString(objs[22]),
                    (Boolean)objs[23],
                    convertToString(objs[24]),
                    convertToString(objs[25]),
                    convertToString(objs[26]),
                    convertToString(objs[27])
            );
            return keywordTypeEntity;
        }
        ).collect(Collectors.toList());
        return entities;
    }
    
    private static String convertToString(Object obj) {
        String value = ObjectUtils.toString(obj, StringUtils.EMPTY);
        return StringUtils.isNotBlank(value) ? value.trim() : "";
    } 
 
    
    
    private static void testEdit(EntityManager em) {
        HrcBranch entity = em.find(HrcBranch.class, 1);
//        entity.getMedias().remove(0);
//        entity.setMedias(new ArrayList());
        entity.setMedias(null);
        em.merge(entity);
    } 
    
    private static void testFind(EntityManager em) {
        HrcBranch entity = em.find(HrcBranch.class, 1);
        System.out.println("Found branch: " + entity.getCurrencyCode());
    }
    
    private static void testPersist(EntityManager em) {
        HrcBranch entity = createBranch();
        em.merge(entity);
    }
    
    private static HrcBranch createBranch() {
        HrcBranch branch = new HrcBranch();
        branch.setCurrencyCode("b1");
        branch.setDescription("Branch 1");
        branch.setIdentifier("b1");
        branch.setIsMainBranch(Boolean.TRUE);
        
//        List<HrcMedia> mediaList = new ArrayList<>();
//        HrcMedia media = new HrcMedia();
//        media.setMediaValue("Phone");
//        media.setTypeCode("Phone");
//        media.setSubtypeCode("Phone 1");
//        media.setBranchId(branch);
//        mediaList.add(media);
//        
//        media = new HrcMedia();
//        media.setMediaValue("Mail");
//        media.setTypeCode("Mail");
//        media.setSubtypeCode("Mail 1");
//        media.setBranchId(branch);
//        mediaList.add(media);
//        
//        branch.setMedias(mediaList);
        return branch;
    }
}
