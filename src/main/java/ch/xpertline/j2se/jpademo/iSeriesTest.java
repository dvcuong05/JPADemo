package ch.xpertline.j2se.jpademo;

import ch.xpertline.component.data.technical.FindRangeCriteria;
import ch.xpertline.j2se.iSeries.entity.PeriodeEntity;
import ch.xpertline.j2se.iSeries.service.PeriodeFacade;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import ch.xpertline.component.data.technical.SearchByCriteria;
import ch.xpertline.component.data.technical.SearchCriteria;
import ch.xpertline.component.data.technical.SearchCriteriaComparisonExpression;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author nhvtuyen
 */
public class iSeriesTest {
    public static void main(String[] args) {
        System.out.println("iSeries test ##################");
        EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("jpa-example");
        EntityManager em = emFactory.createEntityManager();
        em.getTransaction().begin();
        
//        PeriodeEntity en1 = em.find(PeriodeEntity.class, 1L);
        
        PeriodeFacade facade = new PeriodeFacade();
        facade.setEntityManager(em);
        
        PeriodeEntity en1 = findByYear("2016", facade);
        
        System.out.println("PeriodeEntity entity 1=" + en1);
        em.getTransaction().commit();
        em.close();
        emFactory.close();
        System.out.println("iSeries end test ##################");
    }
    
    private static PeriodeEntity findByYear(String year, PeriodeFacade periodeFacade) {

//        Periode periode = null;
        SearchByCriteria searchByCriteria = new SearchByCriteria();
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setExpression(SearchCriteriaComparisonExpression.EQUAL);
        searchCriteria.setValue(year);
        searchCriteria.setField("periodDesignation");
        searchByCriteria.setCriteria(Arrays.asList(searchCriteria));

        FindRangeCriteria range = new FindRangeCriteria();
        range.setFrom(0);
        range.setTo(1);

        List<PeriodeEntity> result = periodeFacade.search(searchByCriteria, null, range);

        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }
}
