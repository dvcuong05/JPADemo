/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.xpertline.j2se.jpademo.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nndiem
 */
@Entity
@Table(name = "hrc_media")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HrcMedia.findAll", query = "SELECT h FROM HrcMedia h")})
public class HrcMedia implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "media_id")
    private Integer mediaId;
    @Basic(optional = false)
    @Column(name = "media_value", columnDefinition = "NVARCHAR(50)")
    private String mediaValue;
      
    @JoinColumn(name = "branch_id", referencedColumnName = "branch_id")
    @ManyToOne
    private HrcBranch branchId;
   
    @Column(name = "type_code")
    private String typeCode;
    
//    @Size(max = 10)
    @Column(name = "subtype_code")
    private String subtypeCode;
     
        
    public HrcMedia() {
    }

    public HrcMedia(Integer mediaId) {
        this.mediaId = mediaId;
    }

    public Integer getMediaId() {
        return mediaId;
    }

    public void setMediaId(Integer mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaValue() {
        return mediaValue;
    }

    public void setMediaValue(String mediaValue) {
        this.mediaValue = mediaValue;
    }
 

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public void setSubtypeCode(String subtypeCode) {
        this.subtypeCode = subtypeCode;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public String getSubtypeCode() {
        return subtypeCode;
    }

    public HrcBranch getBranchId() {
        return branchId;
    }

    public void setBranchId(HrcBranch branchId) {
        this.branchId = branchId;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mediaId != null ? mediaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HrcMedia)) {
            return false;
        }
        HrcMedia other = (HrcMedia) object;
        if ((this.mediaId == null && other.mediaId != null) || (this.mediaId != null && !this.mediaId.equals(other.mediaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ch.xpertline.xhrc.entity.restricted.HrcMedia[ mediaId=" + mediaId + " ]";
    }
    
}
