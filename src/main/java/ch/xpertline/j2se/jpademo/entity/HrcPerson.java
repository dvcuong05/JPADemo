package ch.xpertline.j2se.jpademo.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nndiem
 */
@Entity
@Table(name = "hrc_person")
@XmlRootElement
@NamedQueries({})
public class HrcPerson implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "person_id")
    private Integer personId;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "objectId")
    private List<HrcPersonExternal> externalRefs;

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public List<HrcPersonExternal> getExternalRefs() {
        return externalRefs;
    }

    public void setExternalRefs(List<HrcPersonExternal> externalRefs) {
        this.externalRefs = externalRefs;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personId != null ? personId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HrcPerson)) {
            return false;
        }
        HrcPerson other = (HrcPerson) object;
        if ((this.personId == null && other.personId != null) || (this.personId != null && !this.personId.equals(other.personId))) {
            return false;
        }
        return true;
    }

}
