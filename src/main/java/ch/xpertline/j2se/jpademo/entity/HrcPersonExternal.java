package ch.xpertline.j2se.jpademo.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nndiem
 */
@Entity
@Table(name = "hrc_person_external_ref")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HrcPersonExternal.findByExternalRef", query = "SELECT e FROM HrcPersonExternal e where externalRef=:externalRef"),

})
public class HrcPersonExternal implements Serializable {

    private static long serialVersionUID = 1L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", nullable = false, columnDefinition = "NVARCHAR(50)")
    private String name;
    @Column(name = "external_ref", nullable = false, columnDefinition = "NVARCHAR(256)")
    private String externalRef;
    
    @JoinColumn(name = "object_id", referencedColumnName = "person_id")
    @ManyToOne(optional = false)
    private HrcPerson objectId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

  
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExternalRef() {
        return externalRef;
    }

    public void setExternalRef(String externalRef) {
        this.externalRef = externalRef;
    }

    public HrcPerson getObjectId() {
        return objectId;
    }

    public void setObjectId(HrcPerson objectId) {
        this.objectId = objectId;
    }

 
}
