package ch.xpertline.j2se.jpademo.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author nvhung
 */
@Entity
@Table(name = "ECM_PARAM")
@IdClass(ECMParamId.class)
@NamedQueries({
    @NamedQuery(name = "findByType", query = "SELECT p FROM ECMParameterEntity p WHERE p.paramType = :paramType")
})
public class ECMParameterEntity implements Serializable{    
    @Id
    @Column(name = "param_id", columnDefinition = "varchar(50)", nullable = false)
    private String paramId;
    
    @Id
    @Column(name = "param_type", columnDefinition = "varchar(255)", nullable = false)
    private String paramType;
    
    @Column(name = "param_data", columnDefinition = "varchar(max)", nullable = true)
    private String paramData;
    
    @Column(name = "radiated", columnDefinition = "bit", nullable = true)
    private boolean radiated;
    
    @Column(name = "creation_date", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)    
    private Date creationDate;
    
    @Column(name = "creation_user", columnDefinition = "varchar(255)", nullable = true)
    private String creationUser;
    
    @Column(name = "modification_date", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)    
    private Date modificationDate;
    
    @Column(name = "modification_user", columnDefinition = "varchar(255)", nullable = true)
    private String modificationUser;
    
    public String getParamId() {
        return paramId;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getParamData() {
        return paramData;
    }

    public void setParamData(String paramData) {
        this.paramData = paramData;
    }

    public boolean isRadiated() {
        return radiated;
    }

    public void setRadiated(boolean radiated) {
        this.radiated = radiated;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModificationUser() {
        return modificationUser;
    }

    public void setModificationUser(String modificationUser) {
        this.modificationUser = modificationUser;
    }

}
