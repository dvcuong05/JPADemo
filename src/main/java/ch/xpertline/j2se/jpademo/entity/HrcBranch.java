package ch.xpertline.j2se.jpademo.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nndiem
 */
@Entity
@Table(name = "hrc_branch")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HrcBranch.findAll", query = "SELECT h FROM HrcBranch h")})
public class HrcBranch implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "branch_id")
    private Integer branchId;

//    @NotNull
//    @Size(max = 10)
    @Column(name = "currency_code", length = 10)
    private String currencyCode;

    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 50)
    @Column(name = "description", columnDefinition = "NVARCHAR(50)")
    private String description;
    
//    @Size(min = 0, max = 50)
    @Column(name = "identifier", columnDefinition = "NVARCHAR(50)")
    private String identifier;

    @Column(name = "is_headquarters", nullable = false)
    private Boolean isMainBranch = false;
    
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "branchId")
    private List<HrcMedia> medias;
    
    public HrcBranch() {
    }

    public HrcBranch(Integer branchId) {
        this.branchId = branchId;
    }

    public HrcBranch(Integer branchId, String description) {
        this.branchId = branchId;
        this.description = description;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Boolean getIsMainBranch() {
        return isMainBranch;
    }

    public void setIsMainBranch(Boolean isMainBranch) {
        this.isMainBranch = isMainBranch;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (branchId != null ? branchId.hashCode() : 0);
        return hash;
    }

    public List<HrcMedia> getMedias() {
        return medias;
    }

    /**
     * Need to keep the same instance of medias, should not replace it.
     * @param medias 
     */
    public void setMedias(List<HrcMedia> medias) {
//        this.medias = medias;
        if (this.medias == null) {
            this.medias = new ArrayList<>();
        }
        this.medias.clear();
        if (medias != null) {
            this.medias.addAll(medias);
        }
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HrcBranch)) {
            return false;
        }
        HrcBranch other = (HrcBranch) object;
        if ((this.branchId == null && other.branchId != null) || (this.branchId != null && !this.branchId.equals(other.branchId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ch.xpertline.xhrc.entity.restricted.HrcBranch[ branchId=" + branchId + " ]";
    }

}
