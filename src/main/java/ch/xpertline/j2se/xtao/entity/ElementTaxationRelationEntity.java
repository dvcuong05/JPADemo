/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.xpertline.j2se.xtao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hanguyen
 */
@Entity
@Table(name = "SCKXGT")
@XmlRootElement

public class ElementTaxationRelationEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ElementTaxationPK id;

    @Column(name = "SCKCXA")
    private BigDecimal montantDeterminantLeTaux;

    @Column(name = "SCKXNA")
    private String calculePar;

    /**
     * @return the id
     */
    public ElementTaxationPK getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(ElementTaxationPK id) {
        this.id = id;
    }

    /**
     * @return the montantDeterminantLeTaux
     */
    public BigDecimal getMontantDeterminantLeTaux() {
        return montantDeterminantLeTaux;
    }

    /**
     * @param montantDeterminantLeTaux the montantDeterminantLeTaux to set
     */
    public void setMontantDeterminantLeTaux(BigDecimal montantDeterminantLeTaux) {
        this.montantDeterminantLeTaux = montantDeterminantLeTaux;
    }

    public String getCalculePar() {
        return calculePar;
    }

    public void setCalculePar(String calculePar) {
        this.calculePar = calculePar;
    }

}
