/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.xpertline.j2se.xtao.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author hanguyen
 */
@Embeddable
public class ElementTaxationPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "UF3AA")
    private Integer periodNumber;

    @Basic(optional = false)
    @Column(name = "SC1U5A")
    private Long noSequenceContribuable;

    @Basic(optional = false)
    @Column(name = "SC26GA")
    private Long numeroDossierFiscal;

    @Basic(optional = false)
    @Column(name = "SC26HA")
    private Long numeroDeTaxation;

    @Basic(optional = false)
    @Column(name = "UFQ6A")
    private Long calculationElementID;

    @Basic(optional = false)
    @Column(name = "SC2FVA")
    private String codeGroupeDeRubriques;

    @Basic(optional = false)
    @Column(name = "SC2FWA")
    private Long sequenceElement;

    /**
     * @return the periodNumber
     */
    public Integer getPeriodNumber() {
        return periodNumber;
    }

    /**
     * @param periodNumber the periodNumber to set
     */
    public void setPeriodNumber(Integer periodNumber) {
        this.periodNumber = periodNumber;
    }

    /**
     * @return the noSequenceContribuable
     */
    public Long getNoSequenceContribuable() {
        return noSequenceContribuable;
    }

    /**
     * @param noSequenceContribuable the noSequenceContribuable to set
     */
    public void setNoSequenceContribuable(Long noSequenceContribuable) {
        this.noSequenceContribuable = noSequenceContribuable;
    }

    /**
     * @return the numeroDossierFiscal
     */
    public Long getNumeroDossierFiscal() {
        return numeroDossierFiscal;
    }

    /**
     * @param numeroDossierFiscal the numeroDossierFiscal to set
     */
    public void setNumeroDossierFiscal(Long numeroDossierFiscal) {
        this.numeroDossierFiscal = numeroDossierFiscal;
    }

    /**
     * @return the numeroDeTaxation
     */
    public Long getNumeroDeTaxation() {
        return numeroDeTaxation;
    }

    /**
     * @param numeroDeTaxation the numeroDeTaxation to set
     */
    public void setNumeroDeTaxation(Long numeroDeTaxation) {
        this.numeroDeTaxation = numeroDeTaxation;
    }

    /**
     * @return the calculationElementID
     */
    public Long getCalculationElementID() {
        return calculationElementID;
    }

    /**
     * @param calculationElementID the calculationElementID to set
     */
    public void setCalculationElementID(Long calculationElementID) {
        this.calculationElementID = calculationElementID;
    }

    /**
     * @return the codeGroupeDeRubriques
     */
    public String getCodeGroupeDeRubriques() {
        return codeGroupeDeRubriques;
    }

    /**
     * @param codeGroupeDeRubriques the codeGroupeDeRubriques to set
     */
    public void setCodeGroupeDeRubriques(String codeGroupeDeRubriques) {
        this.codeGroupeDeRubriques = codeGroupeDeRubriques;
    }

    /**
     * @return the sequenceElement
     */
    public Long getSequenceElement() {
        return sequenceElement;
    }

    /**
     * @param sequenceElement the sequenceElement to set
     */
    public void setSequenceElement(Long sequenceElement) {
        this.sequenceElement = sequenceElement;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.periodNumber);
        hash = 89 * hash + Objects.hashCode(this.noSequenceContribuable);
        hash = 89 * hash + Objects.hashCode(this.numeroDossierFiscal);
        hash = 89 * hash + Objects.hashCode(this.numeroDeTaxation);
        hash = 89 * hash + Objects.hashCode(this.calculationElementID);
        hash = 89 * hash + Objects.hashCode(this.codeGroupeDeRubriques);
        hash = 89 * hash + Objects.hashCode(this.sequenceElement);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ElementTaxationPK other = (ElementTaxationPK) obj;
        if (!Objects.equals(this.periodNumber, other.periodNumber)) {
            return false;
        }
        if (!Objects.equals(this.noSequenceContribuable, other.noSequenceContribuable)) {
            return false;
        }
        if (!Objects.equals(this.numeroDossierFiscal, other.numeroDossierFiscal)) {
            return false;
        }
        if (!Objects.equals(this.numeroDeTaxation, other.numeroDeTaxation)) {
            return false;
        }
        if (!Objects.equals(this.calculationElementID, other.calculationElementID)) {
            return false;
        }
        if (!Objects.equals(this.codeGroupeDeRubriques, other.codeGroupeDeRubriques)) {
            return false;
        }
        if (!Objects.equals(this.sequenceElement, other.sequenceElement)) {
            return false;
        }
        return true;
    }
    
}
