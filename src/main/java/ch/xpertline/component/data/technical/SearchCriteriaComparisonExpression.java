package ch.xpertline.component.data.technical;

/**
 *
 * @author nvmuon
 */
public enum SearchCriteriaComparisonExpression {

    EQUAL("equal"),
    LIKE("like"),
    NOT_EQUAL("notequal"),
    GREATER_THAN("gt"),
    GREATER_THAN_OR_EQUAL("ge"),
    LESS_THAN("lt"),
    LESS_THAN_OR_EQUAL("le"),
    IS_NULL("isnull"),
    IS_NOT_NULL("isnotnull"),
    IN("in"),
    BETWEEN("between"),
    NOT_BETWEEN("notbetween"),
    STARTS_WITH("startswith"),
    ENDS_WITH("endswith");

    private String expression;

    private SearchCriteriaComparisonExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public String toString() {
        return this.expression.toLowerCase();
    }

    public String sqlExpression() {
        String value = "";
        if (this.compareTo(EQUAL) == 0) {
            value = "=";
        } else if (this.compareTo(LIKE) == 0) {
            value = "LIKE";
        } else if (this.compareTo(NOT_EQUAL) == 0) {
            value = "<>";
        } else if (this.compareTo(GREATER_THAN) == 0) {
            value = ">";
        } else if (this.compareTo(GREATER_THAN_OR_EQUAL) == 0) {
            value = ">=";
        } else if (this.compareTo(LESS_THAN) == 0) {
            value = "<";
        } else if (this.compareTo(LESS_THAN_OR_EQUAL) == 0) {
            value = "<=";
        } else if (this.compareTo(IS_NULL) == 0) {
            value = "IS NULL";
        } else if (this.compareTo(IS_NOT_NULL) == 0) {
            value = "IS NOT NULL";
        } else if (this.compareTo(IN) == 0) {
            value = "IN";
        } else if (this.compareTo(BETWEEN) == 0) {
            value = "BETWEEN";
        } else if (this.compareTo(NOT_BETWEEN) == 0) {
            value = "NOT BETWEEN";
        } else {
            throw new UnsupportedOperationException("The operation is not supported");
        }
        return value;
    }
    
    public String mongoExpression() {
        String value = "";
        if (this.compareTo(LIKE) == 0) {
            value = "$regex";
        } else if (this.compareTo(SearchCriteriaComparisonExpression.GREATER_THAN) == 0) {
            value = "$gt";
        } else if (this.compareTo(SearchCriteriaComparisonExpression.GREATER_THAN_OR_EQUAL) == 0) {
            value = "$gte";
        } else if (this.compareTo(SearchCriteriaComparisonExpression.LESS_THAN) == 0) {
            value = "$lt";
        } else if (this.compareTo(SearchCriteriaComparisonExpression.LESS_THAN_OR_EQUAL) == 0) {
            value = "$lte";
        } else if (this.compareTo(SearchCriteriaComparisonExpression.NOT_EQUAL) == 0) {
            value = "$ne";
        } else if (this.compareTo(SearchCriteriaComparisonExpression.IN) == 0) {
            value = "$in";
        } else {
            throw new UnsupportedOperationException("The operation is not supported");
        }
        return value;
    }
}
