package ch.xpertline.component.data.technical;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nvhung
 */
public enum ECMParamType {
    DEPARTMENT("DEPARTMENT"),
    DATABASE("DATABASE"),
    EDITOR("EDITOR"),
    DATATYPE("DATATYPE"),
    NATURE("NATURE_TYPE");
    
    private final String value;

    private ECMParamType(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
    
    /**
     * get all document nature criteria fields of group as List of String
     *
     * @return list of document nature criteria fields
     */
    public static List<String> getValuesAsString() {
        ECMParamType[] fields = ECMParamType.values();
        List<String> fieldsAsString = new ArrayList<>();
        for (ECMParamType field : fields) {
            fieldsAsString.add(field.getValue());
        }
        return fieldsAsString;
    }
    
}
