package ch.xpertline.component.data.technical;

import java.io.Serializable;

/**
 * SearchCriteria defines a search pattern (field, value and expression) to
 * restrict the search result
 *
 * @author tifxb
 */
public class SearchCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    public SearchCriteria() {
    }

    public SearchCriteria(String field, Object value, SearchCriteriaComparisonExpression expression) {
        this.field = field;
        this.value = value;
        this.expression = expression;
    }
    /**
     * Field name to search on
     */

    private String field = null;
    /**
     * Value to apply the search expression
     */
    private Object value = null;
    /**
     * SearchCriteriaComparisonExpression to search (equal, ge, le, ...)
     */
    private SearchCriteriaComparisonExpression expression = null;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public SearchCriteriaComparisonExpression getExpression() {
        return expression;
    }

    public void setExpression(SearchCriteriaComparisonExpression expression) {
        this.expression = expression;
    }

    @Override
    public String toString() {
        return this.field + " " + this.expression.toString() + " "
                + value != null ? " " + value.toString() : "";
    }
}
