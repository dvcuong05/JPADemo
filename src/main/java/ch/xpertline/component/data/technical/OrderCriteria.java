package ch.xpertline.component.data.technical;

import java.io.Serializable;

/**
 * OrderCriteria defines an order pattern (field, descending) to sort the result
 *
 * @author tifxb
 */
public class OrderCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    public OrderCriteria() {
    }

    public OrderCriteria(String field, boolean descending) {
        this.field = field;
        this.descending = descending;
    }
    /**
     * Defines the Order field name to apply the sort
     */
    private String field = null;
    /**
     * Defines the descending or ascending order
     */
    private boolean descending = false;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public boolean isDescending() {
        return descending;
    }

    public void setDescending(boolean descending) {
        this.descending = descending;
    }
}
