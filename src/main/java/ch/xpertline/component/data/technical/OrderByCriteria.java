package ch.xpertline.component.data.technical;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * OrderByCriteria defines lists of orderCriteria objects for a sort
 *
 * @author tifxb
 */
public class OrderByCriteria implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<OrderCriteria> order = null;

    public List<OrderCriteria> getOrder() {
        return order;
    }

    public void setOrder(List<OrderCriteria> order) {
        this.order = order;
    }
    
    public void addOrder(OrderCriteria order){
        if(this.order == null){
             this.order = new ArrayList<OrderCriteria>();
        }
        this.order.add(order);
    }
}
