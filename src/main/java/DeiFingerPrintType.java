
import javax.xml.bind.annotation.XmlEnumValue;

public enum DeiFingerPrintType {
    @XmlEnumValue("MD5")
    MD_5("MD5"),
    @XmlEnumValue("SHA1")
    SHA_1("SHA1"),
    @XmlEnumValue("SHA256")
    SHA_256("SHA256");
    private final String value;

    DeiFingerPrintType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeiFingerPrintType fromValue(String v) {
        for (DeiFingerPrintType c: DeiFingerPrintType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}