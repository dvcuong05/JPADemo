
import ch.xpertline.component.data.technical.SearchByCriteria;
import ch.xpertline.component.data.technical.SearchCriteria;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.FilenameUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dvcuong
 */
public class TestMain {
    public static void main(String[] args) {
        /*List<String> permissions1 = new ArrayList<>(Arrays.asList("Permission1","Permission1","Permission2"));
        List<String> permissions2 = new ArrayList<>(Arrays.asList("Permission3","Permission4","Permission2"));
        
        Map<String,List> map1 = new HashMap<String, List>();
        map1.put("classA", permissions1);
        
        Map<String,List> map2 = new HashMap<String, List>();
        map2.put("classB", permissions2);
        
        Map<String,List> map3 = new HashMap<String, List>();
        map3.putAll(map1);
        map3.putAll(map2);
        System.err.println("Hello"+Boolean.valueOf(null));
        System.err.println(map3);*/
        /*SearchByCriteria searchByCriteria = new SearchByCriteria();
        SearchCriteria criteria = new SearchCriteria("type", "database", SearchCriteriaComparisonExpression.LESS_THAN);        
        searchByCriteria.addCriteria(criteria);
        criteria = new SearchCriteria("type", "department", SearchCriteriaComparisonExpression.LESS_THAN);        
        searchByCriteria.addCriteria(criteria);
        criteria = new SearchCriteria("fullText", "fullText", SearchCriteriaComparisonExpression.LESS_THAN);        
        searchByCriteria.addCriteria(criteria);
        
        SearchByCriteria searchByCriteriaOr = new SearchByCriteria();
        criteria = new SearchCriteria("type", "datatype", SearchCriteriaComparisonExpression.LESS_THAN);        
        searchByCriteriaOr.addCriteria(criteria);
        criteria = new SearchCriteria("strike", "true", SearchCriteriaComparisonExpression.LESS_THAN);        
        searchByCriteriaOr.addCriteria(criteria);
        criteria = new SearchCriteria("fullText", "fullText", SearchCriteriaComparisonExpression.LESS_THAN);        
        searchByCriteriaOr.addCriteria(criteria); 
        criteria = new SearchCriteria("type", "database", SearchCriteriaComparisonExpression.LESS_THAN);        
        searchByCriteriaOr.addCriteria(criteria);  
        searchByCriteria.addOr(searchByCriteriaOr);
        
        searchByCriteriaOr = new SearchByCriteria();
        criteria = new SearchCriteria("type", "nature", SearchCriteriaComparisonExpression.LESS_THAN);        
        searchByCriteriaOr.addCriteria(criteria);
        criteria = new SearchCriteria("strike", "true", SearchCriteriaComparisonExpression.LESS_THAN);        
        searchByCriteriaOr.addCriteria(criteria);
        criteria = new SearchCriteria("fullText", "fullText", SearchCriteriaComparisonExpression.LESS_THAN);        
        searchByCriteriaOr.addCriteria(criteria);        
        searchByCriteria.addOr(searchByCriteriaOr);
        
        System.err.println(getSearchCriteriaValuesByFieldName(searchByCriteria, "type"));*/
         String extension = FilenameUtils.getExtension("c:\\cuong\\abc.doc");
         System.err.println("AAAAA:"+extension);
    }
    
    /**
     * Retrieve the list values of searchCriteria has search field match with given parameter
     * @param searchByCriteria
     * @param field
     * @return list of string values
     */
    public static List<String> getSearchCriteriaValuesByFieldName(SearchByCriteria searchByCriteria, String field){
        Set<String> listOfValues =  new HashSet<>();
        
        if(searchByCriteria == null){
            return new ArrayList<>(listOfValues);
        }
        if(searchByCriteria.getCriteria() != null){
            for(SearchCriteria searchCriteria : searchByCriteria.getCriteria()){
                if(field.equalsIgnoreCase(searchCriteria.getField())){
                    listOfValues.add(searchCriteria.getValue().toString());
                }
            }            
            
            if(searchByCriteria.getOr() != null){
              for(SearchByCriteria searchByCriteria1 : searchByCriteria.getOr()){
                   listOfValues.addAll(getSearchCriteriaValuesByFieldName(searchByCriteria1, field));
                };
                return new ArrayList<>(listOfValues);
            }
        }
        return new ArrayList<>(listOfValues);
    }
}

