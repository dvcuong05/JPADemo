

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author nhnhut
 */
public class LogAnalysic {
    public static void main(String[] args) {
        Map<String,List<String>> values = new HashMap<>();
        File file = new File("d:\\FolderECM\\wildfly-10.0.0.Final\\standalone\\log\\server.log");
        try {
            List<String> contents = FileUtils.readLines(file);
            String begin = "XECMIndexationBean on environment Dev002";
            int beginLength = begin.length();
            String end = ". Time cost : ";
            int endLength = end.length();
            // Iterate the result to print each line of the file.
            for (String line : contents) {
                if(line.contains(begin)){
                    String environment = line.substring(line.indexOf(begin) + beginLength, line.indexOf(begin) + beginLength+2);
                    String time = line.substring(line.indexOf(end)+endLength);
                  if(allow(time)){
                    put(values,"Dev002"+environment,time);
                  }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<String> keys = sort(values.keySet());
        int minsize = 0;
        for (String key : keys) {
            System.out.println("Environment " + key + " has "+values.get(key).size()+" values:");   
            for (String string : values.get(key)) {
                System.out.print(string+";");   
            }
            System.out.print(" Average: " + average(values.get(key)));
            
            if(minsize!=0){
                if(minsize>values.get(key).size()){
                    minsize = values.get(key).size();
                }
            }else{
                minsize = values.get(key).size();
            }
             System.out.println("");   
        }
        System.out.println("Total env: "+ keys.size() + " Total time = "+minsize);
    }
    private static boolean allow(String value){
       // return true;
       // if(NumberUtils.isNumber(value)){
            return Integer.parseInt(value) > 3000; //
       // }
       // return false;
    }
    public static int average(List<String> a) {

    Integer total = 0;
    for (String element : a) {
        total += Integer.parseInt(element);
    }

    return total / a.size();

}
    private static void put(Map<String, List<String>> values, String environment, String time) {
        if(!values.containsKey(environment)){
            values.put(environment, new LinkedList<String>());
        }
        values.get(environment).add(time);
    }
    private static ArrayList<String> sort(Set<String> set){
        ArrayList a= new ArrayList<String>(set);
       Collections.sort(a, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return o1.toString().compareToIgnoreCase(o2.toString());
            }
        });
       return a;
    }
}
